package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private BankAccount account;
    private BlockingQueue<Transaction> queue;

    public Consumer(BlockingQueue<Transaction> queue, BankAccount account) {
        this.queue = queue;
        this.account = account;
    }

    @Override
    public void run() {
        synchronized (account) {
            while (true) {
                try {
                    Transaction transaction = queue.take();
                    process(transaction);
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                    break;
                }
            }
            Transaction transaction = queue.poll();
            while (transaction != null) {
                process(transaction);
                transaction = queue.poll();
            }
        }
    }

    private void process(Transaction transaction) {
        switch (transaction._type) {
            case Deposit:
                account.deposit(transaction._amountInCents);
                break;
            case Withdraw:
                account.withdraw(transaction._amountInCents);
                break;
        }
    }
}
