package ictgradschool.industry.concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {

        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        ArrayList<Thread> controlThreads = new ArrayList<Thread>();
        ArrayList<Double> estimatedPis = new ArrayList<Double>();

        for (int i = 0; i < 4; i++) {


            Runnable estimatePi1 = new Runnable() {

                @Override
                public void run() {
                    long numInsideCircle = 0;

                    for (long i = 0; i < (numSamples / 4); i++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }

                    }

                    double estimatedPi = 4.0 * (double) numInsideCircle / (double) (numSamples / 4);

                    estimatedPis.add(estimatedPi);

                }


            };

            Thread estimate = new Thread(estimatePi1);

            estimate.start();

            controlThreads.add(estimate);


        }

        for (Thread elements : controlThreads) {
            try {
                elements.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        double sum = 0;

        for (Double elements : estimatedPis) {
            sum = sum + elements;
        }

        double estimatedPiFinal = sum / 4;


        // TODO Implement this.
        return estimatedPiFinal;
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
