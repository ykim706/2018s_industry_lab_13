package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable  {

    public enum TaskState{
        Initialized, Completed, Aborted
    }

    public PrimeFactorsTask(long n){
        List<Long> longList = new ArrayList<Long>();

    }

    @Override
    public void run() {
        long n;
        for (long factor = 2; factor*factor <= n; factor++) {


            while (n % factor == 0) {
                System.out.print(factor + " ");
                n = n / factor;
            }
        }

    }

    public long n(){return ;}

    public List<Long> getPrimeFactors() throws IllegalStateException{return;}

    public TaskState getState(){return;}
}
