package ictgradschool.industry.concurrency;

public class ExerciseOne {
    public static void main(String[] args) {
        Thread myRunnable = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 1000001; i++ ){
                    System.out.println(i);
                }

            }
        });

        myRunnable.start();

        myRunnable.interrupt();
    }
}
