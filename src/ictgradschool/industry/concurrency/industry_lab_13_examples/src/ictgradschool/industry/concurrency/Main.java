package ictgradschool.industry.concurrency;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        // Create a new instance of MyRunnableTask.
        // We will run this in a separate thread.
        MyRunnableTask myTask = new MyRunnableTask();

        // Create a thread which will run myTask
        Thread myThread = new Thread(myTask);

        // Start the thread running
        myThread.start();

        // Pause the main thread until we press ENTER.
        // Note: This will NOT pause myThread - that will keep running in the background.
        System.out.println("Press ENTER to terminate...");
        System.in.read();

        // Interrupting a thread requests that it terminate. It does not actually stop
        // the thread by itself - we must handle the interrupt request in our own code.
        // However, this IS the way to stop threads - NEVER call the stop() method as
        // doing so may corrupt our data.
        myThread.interrupt();
        System.out.println("Interrupt has been called");

        // Actually wait for myThread to finish.
        myThread.join();

        // Print the result.
        System.out.println("Result is: " + myTask.getI());

    }

}
