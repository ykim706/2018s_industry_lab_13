package ictgradschool.industry.concurrency;

/**
 * This class contains code intended to run in a separate thread.
 * It implements the Runnable interface, which defines a single method,
 * run().
 */
public class MyRunnableTask implements Runnable {

    private long i = 0;

    public long getI() {
        return i;
    }

    /**
     * This method contains the code we intend to run in a
     * separate thread.
     */
    @Override
    public void run() {

        try {

            // loop "infinitely"...
            // we could also use while (!Thread.interrupted()) { ... }
            // which would loop until the thread has been interrupted.
            while (true) {

                // Try to sleep for 100ms. If the thread has been
                // interrupted, this will throw an InterruptedException.
                Thread.sleep(100);

                // Do some complex computational task.
                i++;

            }

        } catch(InterruptedException e) {
            // This will be run when our thread has been interrupted.
            System.out.println("Thread was interrupted");
        }

    }
}
