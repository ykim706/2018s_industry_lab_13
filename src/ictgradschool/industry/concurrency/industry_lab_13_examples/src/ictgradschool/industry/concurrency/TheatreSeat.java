package ictgradschool.industry.concurrency;

/**
 * Represents a single seat in a movie theatre / concert venue / etc.
 * It is coded in such a way as to prevent double bookings.
 *
 * All the methods in here are synchronized. This means that only one thread is
 * allowed to call any of these methods on a single TheatreSeat instance at any one time.
 */
public class TheatreSeat {

    private boolean isBooked = false;
    private String customerName;

    public synchronized String getCustomerName() {
        return customerName;
    }

    // Because book() is synchronized, we can guarantee that the code in here won't be interfered
    // with by someone else's booking.
    public synchronized boolean book(String customer) {
        boolean success = false;

        if (!isBooked) {
            isBooked = true;
            this.customerName = customer;
            success = true;
        }

        return success;
    }

    public synchronized boolean isBooked() {
        return isBooked;
    }
}
