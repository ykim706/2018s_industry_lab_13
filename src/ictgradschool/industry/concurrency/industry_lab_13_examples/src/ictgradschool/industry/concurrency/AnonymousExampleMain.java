package ictgradschool.industry.concurrency;

public class AnonymousExampleMain {

    public static void main(String[] args) {

        // This shows the use of an anonymous inner Runnable class, rather
        // than writing a separate, full object.
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("This was printed in a separate thread!");
            }
        });

        myThread.start();

        // This shows the same thing, but using Java lambda expressions.
        // Not testable, but you can see that the code is a little more
        // concise.
        Thread myOtherThread = new Thread( () -> {

            System.out.println("This was printed in another separate thread!");

        });

        myOtherThread.start();

    }
}
