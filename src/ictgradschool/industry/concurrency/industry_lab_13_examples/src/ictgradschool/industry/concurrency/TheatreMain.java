package ictgradschool.industry.concurrency;

public class TheatreMain {

    public static void main(String[] args) throws InterruptedException {

        TheatreSeat seat = new TheatreSeat();

        Thread bob = new Thread(new Runnable() {
            @Override
            public void run() {

                boolean success = seat.book("Bob");
                System.out.println("Did Bob get the seat? " + success);

            }
        });

        Thread alice = new Thread(new Runnable() {
            @Override
            public void run() {

                boolean success = seat.book("Alice");
                System.out.println("Did Alice get the seat? " + success);

            }
        });

        bob.start();
        alice.start();

        bob.join();
        alice.join();

        System.out.println("Who got the seat? " + seat.getCustomerName());

    }

}
